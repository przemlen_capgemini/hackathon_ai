import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AppMapPage } from '../pages/app-map/app-map';
import { Geolocation } from '@ionic-native/geolocation';
import { AmazonService } from '../services/amazon-service';
import { HttpService } from '../services/http-service';
import { HttpClient, HttpClientModule } from '../../node_modules/@angular/common/http';
import { NativeAudio } from '../../node_modules/@ionic-native/native-audio';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    AppMapPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    AppMapPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    AmazonService,
    HttpService,
    HttpClient,
    NativeAudio,
  ]
})
export class AppModule {}

