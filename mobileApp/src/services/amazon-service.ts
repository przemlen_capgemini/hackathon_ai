import { Injectable } from '../../node_modules/@angular/core';
import { Observable } from '../../node_modules/rxjs/Observable';
import { HttpService } from './http-service';
import { Position } from '../model/postion';
import { Accident } from '../model/accident';
@Injectable()
export class AmazonService {

    private url = "https://ss57xg42u3.execute-api.eu-west-1.amazonaws.com/prod";

    constructor(private http: HttpService) {
    }

    sendPosition(position: Position): Observable<string> {
        return this.http.sendPosition(this.url, position);
    }
}