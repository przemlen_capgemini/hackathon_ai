import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AppMapPage } from '../app-map/app-map';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  places: any;

  constructor(public navCtrl: NavController) {
  
    }
  onLoadMap():void{
    this.navCtrl.push(AppMapPage);
  }


}
