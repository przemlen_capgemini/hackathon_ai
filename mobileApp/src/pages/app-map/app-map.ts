import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { Subscription } from '../../../node_modules/rxjs/Subscription';
import { filter, timestamp } from 'rxjs/operators';
import { AmazonService } from '../../services/amazon-service';
import { Position } from '../../model/postion';
import { Accident } from '../../model/accident';
import { NativeAudio } from '../../../node_modules/@ionic-native/native-audio';

/**
 * Generated class for the AppMapPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
declare var google: any;

@IonicPage()
@Component({
  selector: 'page-app-map',
  templateUrl: 'app-map.html',
})
export class AppMapPage {


  @ViewChild('map') mapElement: ElementRef;
  map: any;
  start: any;
  end: any = '';
  currentPosition: any;
  trackedPosition: any;
  currentMarker: any;
  interval: any;
  positionToAws: Position;
  accident: Accident;
  mp3Path: string = 'assets/mp3/';
  currentAccidentType: string;
  iconPath: string = '/assets/icons/';



  positionSubscription: Subscription;

  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;

  constructor(public navCtrl: NavController,
    public navParams: NavParams, private geolocation: Geolocation, private amazonService: AmazonService,
    private nativeAudio: NativeAudio) {

  }

  ionViewDidLoad() {
    this.initMap()
  }

  autoComplete() {
    //TODO Autocomplete searching;
  }

  getAccidentNotification(nameMp3: string) {
    if (nameMp3.endsWith(".mp3")) {
      let rand = 'uniqueId1' + Math.floor(Math.random()*1000);
      this.nativeAudio.preloadSimple(rand, this.mp3Path + nameMp3).then(() => this.nativeAudio.play(rand), (e) => console.error(e));
    }
  }

  sendLocation() {
    this.positionToAws = new Position();
    this.positionToAws.setLat = String(this.currentPosition.lat);
    this.positionToAws.setLng = String(this.currentPosition.lng);
    this.interval = setInterval(() => {
      this.amazonService.sendPosition(this.currentPosition)
        .subscribe(a => {
          if (a.endsWith(".mp3")) {
            this.getAccidentNotification(a);
            this.currentAccidentType = a.substring(0, a.indexOf(".mp3"));
            console.log('this.currentAccidentType: ' + this.currentAccidentType);
            this.displayIcon();
          } else {
            this.currentAccidentType = 'No_Accident';
            console.log('this.currentAccidentType: ' + this.currentAccidentType);
          }
        })
    }, 20000
    )

  }



  initMap() {
    this.geolocation.getCurrentPosition().then(
      (location) => {
        this.currentPosition = { lat: location.coords.latitude, lng: location.coords.longitude }
        this.map = new google.maps.Map(this.mapElement.nativeElement, {
          zoom: 20,
          center: this.currentPosition,
          timeout: 1000,
          mapTypeControl: false,
          streetViewControl: false,
        });
        this.currentMarker = this.addMarker(this.currentPosition, this.map);
        this.directionsDisplay.setMap(this.map);
      }
    )

  }


  addMarker(position, map) {
    return new google.maps.Marker({
      position,
      map
    });
  }

  calculateAndDisplayRoute(): void {
    this.map.setCenter({ lat: 53, lng: 18 });
    this.directionsService.route({
      origin: this.currentPosition, //this.currentPosition, //mocked origin
      destination: { lat: 51.501476, lng: -0.140634 },
      travelMode: 'DRIVING'
    }, (response, status) => {
      if (status === 'OK') {
        this.directionsDisplay.setDirections(response);
        console.log(response);
      } else {
        window.alert('Directions request failed due to ' + status);
      }
    })
    this.trackPosition();
  }

  trackPosition(): void {

    const watch = this.geolocation.watchPosition()
      .pipe(
        filter(p => p.coords !== undefined))
      .subscribe(
        (data) => {
          setTimeout(() => {
            this.trackedPosition = { lat: data.coords.latitude, lng: data.coords.longitude }
            this.currentMarker.setPosition(this.trackedPosition);
            this.map.setCenter(this.trackedPosition);
            this.currentPosition = this.trackedPosition;
          }
          )
        }
      )
    this.sendLocation();
  }


  displayIcon(): void {
    // document.getElementById('idontfuckingcare').setAttribute("src", this.iconPath + this.currentAccidentType + ".svg");
  }
}

