package com.predictive.lambda.exceptions;

public class LambdaFunctionHandlerException extends RuntimeException {

	public LambdaFunctionHandlerException(String message, Throwable cause) {
		super(message, cause);
	}

	public LambdaFunctionHandlerException(String message) {
		super(message);
	}
	
}
